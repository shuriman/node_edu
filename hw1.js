const colors = require("colors/safe");

const isSimple = (n) => {
	for (let i = 2; i < n; i++) {
		if(n % i == 0){
			return false;
		}
	}	
	return true;
}

const switchLight = (v) => {
	l++;
	if(l > 2){
		l = 0;
	}

	switch(l){
		case 0:
			return colors.red(v);
		case 1:
			return colors.yellow(v);
		case 2:
			return colors.green(v);
		default:
			break;
	}

}


const first = process.argv[2];
const last = process.argv[3];

if(!Number.isInteger(parseInt(first)) || !Number.isInteger(parseInt(last))){
	console.log("Is not Integer error!");
	process.exit();
}

let simpleExists = false;
let l = 0;

for (let i = parseInt(first); i <= parseInt(last); i++) {
	if(isSimple(i)){
		console.log(switchLight(i));
		simpleExists = true;
	}
}

if(!simpleExists){
	console.log("Not exists!");
}