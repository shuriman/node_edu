
	var moment = require('moment'); 

	const EventEmitter = require('events');
	const emitter = new EventEmitter();
	
	
/*
format = "ss-mm-hh-DD-MM-YYYY";
*/

	const waitForTime = (timeout) => {

		const p = setInterval(() => {
			let duration = moment.duration(timeout.diff(moment()));
		
			if(duration.asSeconds() > 0){
				emitter.emit('time', duration, timeout);	
				return;
			}

			emitter.emit('timeout', timeout, p);
		}, 1000);

	}
	
	let format = "ss-mm-hh-DD-MM-YYYY";

	for(let i = 2; i< process.argv.length; i++){
		let timeTo = process.argv[i];
		let end  = moment(timeTo, format);
		waitForTime(end);
	}
	
	emitter.on('timeout', (timeTo, timer) => {
		console.log("Время наступило: " + timeTo.format("ss-mm-hh-DD-MM-YYYY"));
		clearInterval(timer);
	})

	emitter.on('time', (d, timeTo) => {
		const t = d._data;

		const text = 'Осталось: days: ' + t.days + ' ' +
                    'hours: '+ t.hours + ' ' +
                    'minutes: ' + t.minutes + ' ' +
                    'seconds: ' + t.seconds + ' до ' + timeTo.format("ss-mm-hh-DD-MM-YYYY");

		console.log(text);
	})

