const fs = require('fs');

const ACCESS_LOG = './access.log';

const readStream = fs.createReadStream(
	ACCESS_LOG,
	{
		flag: 'r',
		encoding: 'utf-8',
		highWaterMark: 1,
	},
);

let line = '';

readStream.on('data', char => {
	if(char == '\n' || char == '\r'){
		if(line.length > 1){
			const ip = line.split(' ')[0];
			fs.createWriteStream(
				ip, {flags: 'a'}
			).write(line+'\n');
		}
		line = '';
	}else{
		line += char;	
	}
	
});