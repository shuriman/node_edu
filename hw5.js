const http = require('http');
const cluster = require('cluster');
const os = require('os');
const fs = require('fs');
const path = require('path');

var executionDir = __dirname;

listDir = (filePath, res) =>
{
    filePath = path.join(executionDir, filePath, path.sep)
    console.log("path: " + filePath);

    if (fs.existsSync(filePath)){
        if(fs.lstatSync(filePath).isFile())
        {
            const readStream = fs.createReadStream(filePath);
            //console.log("file: " + filePath);
            readStream.pipe(res);

        }else{
            //console.log("here: " + filePath);
            executionDir = filePath;
            const dir = fs.readdirSync(filePath);
            
            parsedDir = "<html>";
            parsedDir += "<a href='..'>..</a></br>";
            dir.forEach((element) => {
                parsedDir += "<a href='" + element + "'>" + element + "</a></br>";
            })
            parsedDir += "</html>";
            res.end(parsedDir);
        }
        
    }
}

if (cluster.isMaster) {
    // cluster.fork();
    console.log(`Master process ${process.pid} is running...`);
    for (let i = 0; i < os.cpus().length; i++) {
        console.log(`Forking process number ${i}`);
        cluster.fork();
    }
} else {
    console.log(`Worker ${process.pid} is running`);
    const filePath = path.join(__dirname, './index.html');

    const server = http.createServer((req, res) => {
        if (req.method != 'GET') {
            res.writeHead(405, 'Method not allowed');
        }else{

            if(req.url === '/')
            {
                executionDir = __dirname;
                listDir('', res);
            }
            else
            {
                listDir(req.url, res);
            }
        }
    });

    server.listen(5555);
}
