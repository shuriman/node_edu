#!/usr/local/bin/node
const fs = require('fs');
const path = require('path');
const readline = require('readline');
const inquirer = require('inquirer');
const yargs = require('yargs');

const options = yargs
    .usage('Usage: -p <path> to file -s <string> for search')
    .option('p', {
        alias: 'path',
        describe: 'Path to the file',
        type: 'string',
        demandOption: true,
    })
    .option('s', {
        alias: 'string',
        describe: 'String for search in file',
        type: 'string',
        demandOption: true,
    }).argv;
//
//console.log(options['path']);

let executionDir = options['path'];
var searchString = new RegExp(options['string']);

console.log("Start in Directory: " + executionDir);
console.log("We'll search this string in file: " + searchString);

listDir = (l) =>
{
    inquirer.prompt([
        {
            name: 'fileName',
            type: 'list', // input, number, confirm, list, checkbox, password
            message: 'Choose a file to read:',
            choices: l,
        },
    ]).then(({ fileName }) => {

        //executionDir = path.join(executionDir, fileName)+"\\";
        executionDir = path.join(executionDir, fileName, path.sep)
        console.log(executionDir);

        if(fs.lstatSync(executionDir).isFile())
        {
            const data = fs.readFileSync(executionDir, 'utf-8');
            if(searchString.test(data)){
                console.log("VALID");
            }
            else{
                console.log("INVALID");
            }
        }else{
            const dir = fs.readdirSync(executionDir).filter(isFile);
            listDir(dir);
        }
        
    });
}

const isFile = (fileName) => fs.lstatSync(executionDir + fileName);
const list = fs.readdirSync(executionDir).filter(isFile);
listDir(list);
